package com.emilia.honestsign.config;

import com.google.common.util.concurrent.RateLimiter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class ConfigurationDoc {

    @Bean
    public RateLimiter rateLimiter(){
        return RateLimiter.create(1,Duration.ofMinutes(1));
       // return RateLimiter.create(,Duration.ofMinutes(2));
        //return  RateLimiter.create(0.1d, Duration.ofSeconds(30));
    }


}
