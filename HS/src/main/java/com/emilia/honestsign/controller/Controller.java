package com.emilia.honestsign.controller;

import ch.qos.logback.core.model.Model;
import com.emilia.honestsign.model.Document;


import com.google.common.util.concurrent.RateLimiter;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/api")
public class Controller {
    private final RateLimiter rateLimiter;

    // String serverUrl="http://localhost:8000/api/postDocument";
    @PostMapping("/postDocument")
public  String createDocument( Document document){


        boolean oktogo = rateLimiter.tryAcquire();
        if (oktogo){

           return document.create()+new ResponseEntity<Document>(HttpStatus.CREATED);

        }
        else
            return String.valueOf(new ResponseEntity<Document>(HttpStatus.TOO_MANY_REQUESTS));


    }



}
