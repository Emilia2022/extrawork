package com.emilia.honestsign.model;


import jakarta.validation.constraints.NotEmpty;
import lombok.*;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Value
@Builder

public class Document  {

    @NotEmpty(message = "proved document format")
    private String document_format;
    @NotEmpty(message = "proved product document ")
   private String product_document;
    private String product_group;
    @NotEmpty(message = " proved signature")
   private String signature;
    @NotEmpty(message = "proved type")
    private String type;


    private static final Map DOCUMENTS_STORE = new ConcurrentHashMap();
    public String create( ){
        String token = UUID.randomUUID().toString();
        Document document1=Document.builder()
                .document_format("json")
                .product_document("Base64")
                .product_group("pharma")
                .signature("signature")
                .type("AGGREGATION_DOCUMENT")
                .build();
        DOCUMENTS_STORE.put(document1,token);
        return "value : "+ token;
    }

}
